<?php
$host = 'localhost';
$user = 'root';
$password = '';
$db = 'prueba';
$query = "";

$link = mysqli_connect($host, $user, $password, $db); // or die('conexion no exitosa');
if(mysqli_connect_errno()){
    echo mysqli_connect_errno();
}
mysqli_set_charset($link, 'utf8_spanish_ci');

getData();

function get(string $table, $Fields = "", $where = "", $limit = 0)
{

    $sql = "SELECT ";
    if($table != "")
    {
        if(gettype($Fields) == 'array')
        {
            foreach($Fields as $Field)
            {
                $sql .= $Field . ', ';
            }
            $sql = substr($sql, 0, strlen($sql)-2);
        }
        else
        {
            $sql .= "*";
        }
        $sql .= " FROM $table";
        if(gettype($where) == 'array')
        {
            $sql .= ' WHERE ';
            foreach($where as $wh)
            {
                $sql .= $wh . 'AND ';
            }
            $sql = substr($sql, 0, strlen($sql)-4);
        }
        if($limit != 0)
        {
            $sql .= ' LIMIT ' . $limit;
        }
        $result = mysqli_query($GLOBALS['link'], $sql);
        $total = [];
        while($row = mysqli_fetch_assoc($result))
        {
            $total[] = $row;
        }
        return $total ;
    }
}

function insert(string $table, $Fields = "", $values = ""){
    $sql = "INSERT INTO ";
    if($table != "" && $values != "")
    {
        $sql .= $table;
        if(gettype($Fields) == 'array')
        {
            $sql .= '(';
            foreach($Fields as $F)
            {
                $sql .= $F . ', ';
            }
            $sql = substr($sql, 0, strlen($sql)-2);
            $sql .= ') ';
        }
        $sql .= ' VALUES (';

        if(gettype($Fields) == 'string')
        {
            $sql .= 'null, ';   
        }

        if(gettype($values) == 'array')
        {
            foreach($values as $v)
            {
                $sql .= "'" . $v . "'" . ', ';
            }
            $sql = substr($sql, 0, strlen($sql)-2);
        }
        $sql .= ');';
        $result = mysqli_query($GLOBALS['link'], $sql);
    }
}

function getData(){
    $table = $_POST['table'];
    unset($_POST['table']);
    
    insert($table, "", $_POST);
}

echo '<pre>';
print_r(get('usuarios'));

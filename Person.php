<?php
declare(strict_types=1);
class Person{
     protected $name;
     protected $lastName;
     protected $age;
     protected $address;

    function __construct()
    {
        
    }
    function set(string $name, string $lastName, int $age, string $address)
    {
        $this->name = $name;
        $this->lastName = $lastName;
        $this->age = $age;
        $this->address = $address; 
    }
    function show()
    {
        echo "Nombre: $this->name \nApellido: $this->lastName\nEdad:$this->age\nDireccion: $this->address";
    }
    function __destruct()
    {

    }
}
$main = new Person();
$main->set('Karen', 'Quiceno', '18', 'CASA');
$main->show();